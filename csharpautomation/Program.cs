using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharpautomation
{
	public class Program
	{
		static void Main(string[] args)
		{
			// The code provided will print ‘Hello World’ to the console.
			// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
			Console.WriteLine(getMessage());
			Console.ReadKey();
		}

		public static String getMessage()
		{
			return "Hello world!!!";
		}

		public static int getSumOf(int a, int b)
		{
			return a + b;
		}

	}
}
