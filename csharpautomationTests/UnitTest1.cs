﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace csharpautomationTests
{
	[TestClass]
	public class UnitTest1
	{
		
		[TestMethod]
		public void TestMethod1()
		{
			Assert.AreEqual("Hello world!!!", csharpautomation.Program.getMessage());
		}
		
		[TestMethod]
		public void TestMethod2()
		{
			Assert.AreEqual("Hello world", csharpautomation.Program.getMessage());
		}

		[TestMethod]
		public void TestMethod3()
		{
			Assert.AreEqual(3, csharpautomation.Program.getSumOf(1, 2));
		}
	}
}
